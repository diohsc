-- This file is part of Diohsc
-- Copyright (C) 2020 Martin Bays <mbays@sdf.org>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of version 3 of the GNU General Public License as
-- published by the Free Software Foundation, or any later version.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see http://www.gnu.org/licenses/.

-- | Stupid module to avoid orphan instances, redefining Fingerprint from
-- x509-validation to have an Ord instance.
module Fingerprint (Fingerprint(..), fingerprint) where

import           Data.ASN1.Types      (ASN1Object)

import qualified Data.ByteString      as BS
import qualified Data.X509            as X
import qualified Data.X509.Validation as X

newtype Fingerprint = Fingerprint BS.ByteString
    deriving (Eq,Ord,Show)

getFingerprint :: (Show a, Eq a, ASN1Object a) => X.SignedExact a -> X.HashALG -> Fingerprint
getFingerprint s a = let X.Fingerprint fp = X.getFingerprint s a in Fingerprint fp

fingerprint :: X.SignedCertificate -> Fingerprint
fingerprint = (`getFingerprint` X.HashSHA256)
