VERSION=0.1.15

GHCOPTS=-threaded -DICONV -DMAGIC -ignore-package regex-compat-tdfa

.PHONY: warn install build
warn:
	@echo "Use \"make install\" to download and compile dependencies and install diohsc"
install: *.hs
	cabal update && cabal install
build: *.hs
	cabal build

diohsc: *.hs
	cabal install -O2 --installdir . --overwrite-policy=always

diohsc.1: diohsc.1.md
	pandoc --standalone -f markdown -t man < diohsc.1.md | sed 's/\$$VERSION/${VERSION}/g' >| diohsc.1

dist-newstyle/sdist/diohsc-${VERSION}.tar.gz: *.hs README.md CHANGELOG.md COPYING *.cabal *.sample diohsc.1
	cabal sdist

diohsc-${VERSION}-src.tgz: dist-newstyle/sdist/diohsc-${VERSION}.tar.gz
	cp $< $@

diohsc.bundle: .git/refs/heads/master
	        git bundle create "$@" HEAD master

index.gmi: index.gmi.in Makefile
	cat $< | sed 's/\$$VERSION/${VERSION}/g' > $@

index.html: index.gmi
	cat $< | sed s/\.gmi/.html/g | ./tools/gmi2html.sed > $@

index.md: index.gmi
	cat $< | sed s/\.gmi/.md/g | ./tools/gmi2md.sed > $@

%.md: %.gmi
	./tools/gmi2md.sed < $< > $@

%.html: %.gmi
	./tools/gmi2html.sed < $< > $@

publish: diohsc-${VERSION}-src.tgz diohsc.bundle index.gmi index.html README.md README.gmi README.html CHANGELOG.gmi CHANGELOG.md CHANGELOG.html tutorial/diohsc-tutorial.cast tutorial/diohsc-tutorial.txt
	cp $^ /var/gemini/gemini.thegonz.net/diohsc/
	scp $^ sverige:html/diohsc/
