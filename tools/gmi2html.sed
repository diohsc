#!/bin/sed -Ef
# gmi2html: Sed script to convert text/gemini to html.
# Based on v0.14.2 of the gemini spec.
#
# This script is dedicated to the public domain according to the terms of CC0:
# https://creativecommons.org/publicdomain/zero/1.0/

# TODO: add meta charset header, I suppose, to ensure utf8?

s/\&/\&amp;/g
s/"/\&quot;/g
s/</\&lt;/g
/^=?>/! s/>/\&gt;/g
/^=?>/ {
    :loop
    s/^((=?>).*)>/\1\&gt;/
    t loop
}

1 {
    i\
<!DOCTYPE html>\
<html>
    h
    s/^#\s*//
    t title_set
    s/.*/Untitled/
    :title_set
    s/.*/<head><title>&<\/title><\/head>/
    p
    i\
<body>
    s/.*//
    x
}

x
/^```/ {
    x
    /^```/ {
        s/.*/<\/pre>/
        h
    }
    b end
}
/^(\*|=>) / {
    x
    /^(\*|=>) / {
        s/^\* (.*)/<li>\1<\/li>/
        s/^=>\s*([^[:space:]]+)\s*$/<li><a href="\1">\1<\/a><\/li>/
        s/^=>\s*([^[:space:]]+)\s+(.+)/<li><a href="\1">\2<\/a><\/li>/
        b end
    }
    i\
</ul>
    h
}
g

t linetypes
:linetypes

s/^```.*/<pre>/

s/^\* (.*)/<ul><li>\1<\/li>/

s/^=>\s*([^[:space:]]+)\s*$/<ul><li><a href="\1">\1<\/a><\/li>/
s/^=>\s*([^[:space:]]+)\s+(.+)/<ul><li><a href="\1">\2<\/a><\/li>/

s/^###\s*(.*)/<h3>\1<\/h3>/
s/^##\s*(.*)/<h2>\1<\/h2>/
s/^#\s*(.*)/<h1>\1<\/h1>/

s/^>\s*(.*)/<blockquote>\1<\/blockquote>/

t end
s/.+/<p>&<\/p>/

:end
$ {
    x
    /^```/ a\
</pre>
    /^(\*|=>) / a\
</ul>
    x
    a\
</body>\
</html>
}
