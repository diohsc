-- This file is part of Diohsc
-- Copyright (C) 2020-23 Martin Bays <mbays@sdf.org>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of version 3 of the GNU General Public License as
-- published by the Free Software Foundation, or any later version.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see http://www.gnu.org/licenses/.

module ClientOptions where

import           System.IO

import           GeminiProtocol (RequestContext)

-- |Immutable options set at startup
data ClientOptions = ClientOptions
    { cOptUserDataDir    :: FilePath
    , cOptInteractive    :: Bool
    , cOptAnsi           :: Bool
    , cOptGhost          :: Bool
    , cOptRestrictedMode :: Bool
    , cOptRequestContext :: RequestContext
    , cOptLogH           :: Maybe Handle
    }
