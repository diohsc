-- This file is part of Diohsc
-- Copyright (C) 2020-23 Martin Bays <mbays@sdf.org>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of version 3 of the GNU General Public License as
-- published by the Free Software Foundation, or any later version.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see http://www.gnu.org/licenses/.

module Target where

import           History
import           Queue
import           URI

data Target
    = TargetHistory HistoryItem
    | TargetFrom HistoryOrigin URI
    | TargetIdUri String URI
    | TargetUri URI

targetUri :: Target -> URI
targetUri (TargetHistory item) = historyUri item
targetUri (TargetFrom _ uri)   = uri
targetUri (TargetIdUri _ uri)  = uri
targetUri (TargetUri uri)      = uri

targetQueueItem :: Target -> QueueItem
targetQueueItem (TargetFrom o uri)   = QueueURI (Just o) uri
targetQueueItem (TargetHistory item) = QueueHistory item
targetQueueItem i                    = QueueURI Nothing $ targetUri i
