% DIOHSC(1) Version $VERSION | diohsc
# NAME
diohsc - line-based gemini client

# SYNOPSIS
diohsc [OPTION]... [URI|PATH]

# DESCRIPTION
diohsc is a gemini client with the following features:

* Simple line-based command-response terminal user interface with ANSI colour.
* Terse but combinatorially expressive command language.
* Navigational aids: history, marks, queue.
* Facilities to invoke external commands and use per-scheme proxies.
* Responses streamed to pager or external command.
* Certificate checking with TOFU and/or explicitly trusted CAs.
* Support for TLS resuming, 0RTT, and client certificates.
* All configuration and certificates stored as user-manipulable files.
* Extensive help.

Please use the "help" and "commands" commands for information on the commands
diohsc understands.

# OPTIONS
-d, \-\-datadir *PATH*
: Directory for configuration and storage of information between sessions.
: Default: ~/.diohsc

-e, \-\-command *COMMAND*
: Execute diohsc command.
: "-e" and "-f" options will be processed in order of appearance,
: after any diohscrc file, and before processing any uri/path argument.

-f, \-\-file *PATH*
: Execute commands from file ("-" for stdin).
: "-e" and "-f" options will be processed in order of appearance,
: after any diohscrc file, and before processing any uri/path argument.

-p, \-\-prompt
: Interactively read diohsc commands from stdin,
: after any commands given as arguments have been executed.
: This is the default unless "-e" or "-f" or "-b" is used.

-i, \-\-interact
: When processing commands, prompt for further input where appropriate.
: This is the default mode unless "-e" or "-f" is used and "-p" is not.

-b, \-\-batch
: When processing commands, never prompt for further input.
: This is the default mode if "-e" or "-f" is used and "-p" is not.

-c, \-\-colour
: Use ANSI escape sequences for formatting.
: This is the default if stdout is a terminal.

-C, \-\-no-colour
: Do not output ANSI escape sequences.
: This is the default if stdout is not a terminal.

-g, \-\-ghost
: "Ghost mode". Do not write logs, input history, or marks to *datadir*, and
: don't write or update "known\_hosts" certificates. The "save" command will
: still write to the filesystem, as will the "identity" command if it is used to
: create a new named identity.

-r, \-\-restricted
: Disallow shell and filesystem access. This prevents all use of external
: commands, and reading or writing outside of *datadir*.

-S, \-\-socks-host *HOST*
: Use SOCKS5 proxy for all connections (including DNS resolution).

-P, \-\-socks-port *PORT*
: Port to use for SOCKS5 proxy. Default: 1080.

-v, \-\-version
: Show version information.

-h, \-\-help
: Show usage information.

# FILES
~/.diohsc
: Default *datadir*. The paths given below assume that this is used.

~/.diohsc/diohscrc
: Configuration file, consisting of diohsc commands to run at startup.

~/.diohsc/log
: Visited uris, one per line.

~/.diohsc/commandHistory
: Commands entered in interactive mode.

~/.diohsc/inputHistory
: Input entered for gemini queries.

~/.diohsc/known\_hosts/
: Contains Trust-On-First-Use trusted certificates of visited hosts.

~/.diohsc/trusted\_certs/
: Certificates will be trusted without warning and without being placed in
: known\_hosts if they are presented with a valid certificate chain from a
: Certificate Authority certificate found in this directory.

~/.diohsc/marks/
: Contains marks for use with the "'" target specifier. Each file contains a
: URI, optionally appended by [IDENT] where IDENT is the name of an identity.

~/.diohsc/identities/
: Contains client certificates and corresponding private keys for named
: cryptographic identities, as produced by the "identify" command.
: These are stored in standard PEM/DER format, so can be imported/exported
: from/to other clients.

~/.diohsc/queue
: Contains URIs, one per line. On startup and before processing each command,
: the contents of this file is added to the queue and the file is deleted. On
: exit, any remaining queue items are appended to this file.

# ENVIRONMENT
BROWSER
: Default browser used by "browse" command without an argument.

PAGER
: Default pager used by "||" command without an argument.

# BUGS
Only ANSI escape sequences are supported.

# LICENCE
diohsc is free software, released under the terms of the GNU GPL v3 or later.
You should have obtained a copy of the licence as the file COPYING.

# AUTHORS
Martin Bays <mbays@sdf.org>
