-- This file is part of Diohsc
-- Copyright (C) 2020-23 Martin Bays <mbays@sdf.org>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of version 3 of the GNU General Public License as
-- published by the Free Software Foundation, or any later version.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see http://www.gnu.org/licenses/.

module ClientState where

import           Control.Monad.State (StateT, modify)
import           Text.Regex          (Regex)

import qualified Data.Map            as M
import qualified Data.Set            as S
import qualified Data.Text.Lazy      as T

import           ActiveIdentities
import           Alias
import qualified BStack
import           CommandLine
import           History
import           Marks
import           Queue
import           Request
import           TextGemini          (PreOpt (..))

data ClientConfig = ClientConfig
    { clientConfDefaultAction     :: (String, [CommandArg])
    , clientConfProxies           :: M.Map String Host
    , clientConfGeminators        :: [(String,(Regex,String))]
    , clientConfRenderFilter      :: Maybe String
    , clientConfPreOpt            :: PreOpt
    , clientConfLinkDescFirst     :: Bool
    , clientConfMaxLogLen         :: Int
    , clientConfMaxWrapWidth      :: Int
    , clientConfNoConfirm         :: Bool
    , clientConfVerboseConnection :: Bool
    }

defaultClientConfig :: ClientConfig
defaultClientConfig = ClientConfig ("page", []) M.empty [] Nothing PreOptPre False 1000 80 False False

data ClientState = ClientState
    { clientCurrent          :: Maybe HistoryItem
    , clientJumpBack         :: Maybe HistoryItem
    , clientLog              :: BStack.BStack T.Text
    , clientVisited          :: S.Set Int
    , clientQueues           :: QueueMap
    , clientActiveIdentities :: ActiveIdentities
    , clientMarks            :: Marks
    , clientSessionMarks     :: M.Map Int HistoryItem
    , clientAliases          :: Aliases
    , clientConfig           :: ClientConfig
    }

type ClientM = StateT ClientState IO

type CommandAction = HistoryItem -> ClientM ()

emptyClientState :: ClientState
emptyClientState = ClientState Nothing Nothing BStack.empty S.empty M.empty M.empty emptyMarks M.empty defaultAliases defaultClientConfig

modifyCConf :: (ClientConfig -> ClientConfig) -> ClientM ()
modifyCConf f = modify $ \s -> s { clientConfig = f $ clientConfig s }

modifyQueues :: (QueueMap -> QueueMap) -> ClientM ()
modifyQueues f = modify $ \s -> s { clientQueues = f $ clientQueues s }
