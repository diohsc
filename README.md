# Diohsc: Denotationally Intricate Obedient Haskell Smallnet Client
diohsc [URI]

## Features
* Simple line-based command-response terminal user interface with ANSI colour.
* Terse but combinatorially expressive command language.
* Navigational aids: history, marks, queue.
* Facilities to invoke external commands and use per-scheme proxies.
* Responses streamed to pager or external command.
* Certificate checking with TOFU and/or explicitly trusted CAs.
* Support for TLS resuming, 0RTT, and client certificates.
* All configuration and certificates stored as user-manipulable files.
* Extensive help.

## Building
Install the haskell package manager cabal; e.g. on a debian system:
```
$ sudo apt-get install cabal-install
```
Then in the diohsc directory, run
```
$ cabal update && cabal install
```
The resulting binary will be installed as `~/.cabal/bin/diohsc`.

### Compile-time options
* `cabal install -f magic`: use libmagic to detect mimetypes of local files
* `cabal install -f irilinks`: allow IRIs in gemtext links

## Trusting server certificates
Diohsc uses "Trust On First Use": if a host provides a certificate chain you do not currently trust, the client will ask for confirmation then add the certificate for the host to a collection stored in `~/.diohsc/known_hosts/` if there isn't already one stored; if there is one already stored, it will give some details and ask if you want to overwrite it with the newly provided one.

You can also trust Certificate Authorities by putting their certificates in `~/.diohsc/trusted_certs/`. For example, if you want to trust all the certificates installed on your system,
```
$ ln -s /etc/ssl/certs/* ~/.diohsc/trusted_certs/
```
or if you just want to trust Let's Encrypt:
```
$ mkdir ~/.diohsc/trusted_certs
$ wget -O ~/.diohsc/trusted_certs/lets-encrypt-x3-cross-signed.pem https://letsencrypt.org/certs/lets-encrypt-x3-cross-signed.pem
$ sha256sum ~/.diohsc/trusted_certs/lets-encrypt-x3-cross-signed.pem
e446c5e9dbef9d09ac9f7027c034602492437a05ff6c40011d7235fca639c79a /home/me/.diohsc/trusted_certs/lets-encrypt-x3-cross-signed.pem
```

## Configuration
See diohscrc.sample for ideas for configuration options, in particular indicating how to make use of third-party software to allow diohsc to access gopher and the web, parse markdown and html, and provide ascii-art previews of image files. Copy the file to ~/.diohsc/diohscrc and uncomment lines as appropriate.

## Multiple instances
Multiple simultaneous diohsc instances may share ~/.diohsc without serious conflict. However, command history is written only on exit, so the last instance to exit will determine that history. The log is appended to continuously, but read only on startup, so logs from simultaneous instances will be interleaved. Uris in ~/.diohsc/queue are taken by an instance on startup and whenever it processes a command, and leftover queue items are written there on exit.

To ensure that an instance won't interfere with other running instances: either use "--datadir" to specify an alternative directory to ~/.diohsc, or use the "--ghost" option (which also acts as a "private browsing" mode).

## Running under Windows
It is apparently possible to compile and run diohsc on Windows. However, it requires a terminal with ANSI and unicode support, which it seems is not the default situation in even the most recent versions of Windows. On Windows 10, following the instructions of this link has been reported to work: 
[https://akr.am/blog/posts/using-utf-8-in-the-windows-terminal](https://akr.am/blog/posts/using-utf-8-in-the-windows-terminal)
