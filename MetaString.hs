-- This file is part of Diohsc
-- Copyright (C) 2020 Martin Bays <mbays@sdf.org>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of version 3 of the GNU General Public License as
-- published by the Free Software Foundation, or any later version.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see http://www.gnu.org/licenses/.

{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE Safe              #-}

-- | Wrap Monoid and IsString into a single typeclass, for convenient
-- polymorphism between Text and String
module MetaString (MetaString, fromString) where

import           Data.String    (IsString, fromString)
import qualified Data.Text.Lazy as T

class (Monoid a, IsString a) => MetaString a

instance MetaString String
instance MetaString T.Text
