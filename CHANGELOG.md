# Changelog for diohsc
This file covers only non-trivial user-visible changes;
see the git log for full gory details.

# 0.1.15
* Switch tls dependency to tls-2.0; deprecated ciphers dropped
* Handle 44 response with exponential backoff
* Fix regression wherein queue files weren't read after start
* Fix handling of tabs

# 0.1.14
* Run $EDITOR on input/query ending with backslash
* Strip all control characters (including ANSI formatting) outside preformatted blocks

# 0.1.13
* Set CLIENT_CERT and CLIENT_KEY environment variables when invoking external commands
* Improve wrapping of lines with words longer than the screen width

# 0.1.12
* SECURITY FIX: Restrict TOFU-trust of a certificate to a single service. Previous behaviour could be exploited for a MitM attack, by convincing the user to accept a certificate for one host, then using it in a MitM for another host.
* Immediately run commands entered in pager mode
* Wrap title lines
* Tell user if a certificate has been temporarily trusted before

# 0.1.11
* Drop targets of "log" from queues
* Preserve history when using relative links (e.g. "2?foo")
* Document backslash escapes in query

# 0.1.10
* Allow pre-fetched items in queue, with new "fetch" command to add them
* Add named queues, e.g. foo~
* Implement notation for (n-from) last element of a list, e.g. _$, _$2, _5-$2
* Run default action on a history item when going to it, e.g. with <
* Don't shell-quote explicit '%s' arguments, only implicit final argument

# 0.1.9
* Improve wrapping and paging with long words, wide chars, or thin terminals
* Shell-quote arguments to 'browse' and '!'
* Save uri with null last path segment under penultimate segment / hostname
* Sort file:// directory listings

# 0.1.8
* Use standard PEM/DER format for identities, allowing import/export
* Optionally generate Ed25519 client certificates ("TARGET id NAME ed")
* Require confirmation before following redirect into scope of identity
* TOFU-trust by default even if there are V3 or NameMismatch errors

# 0.1.7
* Handle ^C during streaming by truncating the stream
* Always request when going to uris using an identity

# 0.1.6.1
* Use canonical notBefore (00:00:00 1 Jan 1950) in client certs
* Add --prompt option, enabling usual command prompt after -e/-f
* Fix query escaping
* Fix colouring of wrapped link descriptions being lost when paging
* Fix --ghost not affecting queue loading

# 0.1.6
* New command "query" for e.g. convenient use of search engines
* Allow IRIs in user input (converted to URIs)
* Implement SOCKS5 support (-S and -P options)
* Fix bug triggered by /home being a symlink
* Fix bug with connecting to literal IPv6 addresses

# 0.1.5
* Align and wrap link lines; add option to print description first
* Use reverse video in prompt for added visibility (thanks Ben)
* Try all addresses when making connections (thanks rwv)
* Improve behaviour in non-interactive mode; add --batch to enable it
* Fix X.509 version and use dummy notAfter in generated client certificates
* Fix bugs in wrapping and relative link display

# 0.1.4
* Indicate links to cached history items
* Retry with full handshake if session resume fails
* Recommend trust for new certificate signed by previous certificate
* Increase client cert validity to 50y
* Make ghost mode even more spectral

# 0.1.3
* Allow trusting certificates just for the current session
* Don't require tail certificates to be v3
* Add uri of any request to log, even if request fails
* Just show fingerprint of known cert rather than picture

# 0.1.2
* Add @ target modifier for history root
* Understand e.g. ~<
* Suppress alt text by default
* Fix: uri quoting for queries and file:// was incorrect
* Fix: queue was not appended to on exit in non-interactive mode
